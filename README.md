# Script post-installation d'un serveur Centos 7

(c) Niki Kovacs, 2020
- Traduction par Mickael Rigonnaux - 2020

Ce repository fourni un script d'installation pour des serveurs Centos 7.
Il permet de mettre en place des configurations et d'installer des outils 
pour faciliter l'utilisation des services.

## Lancement du script

Il faut réaliser les actions suivantes

  1. Installer Centos 7 dans sa version minimale

  2. Créer un utilisateur (non root) avec les droits d'administration

  3. Installer Git: `sudo yum install git`

  4. Télécharger le script: `git clone https://gitlab.com/tzkuat/centos-7.git`

  5. Rentrer dans le dossier: `cd centos-7`

  6. Lancer le script: `sudo ./centos-setup.sh --setup`

  7. Prendez un tasse de thé pendant le script fait le travail.

  8. Redémarrer la machine.


## Personnalisation d'un serveur CentOS

Transformer une installation manimale en un serveur fonctionnal peut prendre du temps selon les opérations.
Voici ce que je fais sur mes installations minimale :

  * Personnaliser le shell Bash : alias, prompt, etc.

  * Personnalisation de VIM

  * Configuration des repository

  * Installation des outils en ligne de commande

  * Désinstallation des paquets non utiles

  * Autoriser l'utilisateur administrateur à accéder aux logs

  * Désactivation d'IPv6 et modification des services qui en dépendent
  
  * Mise en place d'un mot de passe persistant pour sudo

  * Etc.

Le script `centos-setup.sh` permet de réaliser ces actions.

Configuration de Bash et de VIM avec une meilleure résolution:

```
# ./centos-setup.sh --shell
```

Configuration des repository (officel et non officiel):

```
# ./centos-setup.sh --repos
```

Installation des listes de paquet `Core` & `Base` avec d'autres paquets en extra:

```
# ./centos-setup.sh --extra
```

Désinstallation des paquets inutiles:

```
# ./centos-setup.sh --prune
```

Activation de l'accès à l'administrateur au log:

```
# ./centos-setup.sh --logs
```

Désactivation d'IPv6 et configuration des services:

```
# ./centos-setup.sh --ipv4
```

Configuration du mot de passe persistant pour sudo:

```
# ./centos-setup.sh --sudo
```

Toutes les actions à la fois:

```
# ./centos-setup.sh --setup
```

Retour au système initial:

```
# ./centos-setup.sh --strip
```

Affichier le message d'aide:

```
# ./centos-setup.sh --help
```

Si vous voulez afficher les log du script, vous pouvez les voir avec la commande suivante:

```
$ tail -f /tmp/centos-setup.log
```

